variable "gitlab_runner_tags" {
  description = "A comma separated list of tags for the runner"
}

variable "gitlab_runner_name" {
  description = "The name of the gitlab runner to identify in the UI"
}

variable "gitlab_runner_registration_token" {
  description = "The intial project registration token"
}

variable "ssh_public_key" {
  description = "Public ssh key for the instance"
}

variable "public_key_path" {
 description = <<DESCRIPTION
 Path to the SSH public key.
 Ensure this keypair is added
 to your local SSH agent so provisioners
 can connect.
Example: ~/.ssh/my_key.pub
DESCRIPTION
}

variable "key_name" {
  description = "Name of your ssh key"
}